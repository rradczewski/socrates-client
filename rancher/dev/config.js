// @flow
/* eslint-disable */
import type {Config} from '../../src/ConfigType';

const config: Config = {
  environment: 'dev',
  jwtSecret: '%jwt_secret%',
  siteUrl: 'http://35.158.56.167/attendee',
  serverBackend: '/server/api/v1',
};
export default config;
