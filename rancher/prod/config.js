// @flow
/* eslint-disable */
import type {Config} from '../../src/ConfigType';

const config: Config = {
  environment: 'prod',
  jwtSecret: '%jwt_secret%',
  siteUrl: 'https://www.socrates-conference.de/attendee',
  serverBackend: '/server/api/v1'
};
export default config;
