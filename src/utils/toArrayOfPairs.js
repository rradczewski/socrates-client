const newToPairs = (acc, item, index) => {
  if (index % 2 !== 0) {
    acc[acc.length - 1].push(item);
  } else {
    acc.push([item]);
  }
  return acc;
};

const toArrayOfPairs = array => array && array.reduce(newToPairs, []);

export default toArrayOfPairs;