// @flow

import config from './config';

describe('config', () => {
  it('contains environment definition', () => {
    expect(config.environment).toBeDefined();
  });
  it('defines site url', () => {
    expect(config.siteUrl).toBeDefined();
  });
  it('contains server backend path', () => {
    expect(config.serverBackend).toBeDefined();
  });
  it('contains jwt secret', () => {
    expect(config.jwtSecret).toBeDefined();
  });
});
