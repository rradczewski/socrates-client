import './setup';
import {JSDOM} from 'jsdom';

export default function () {
  global.jsdom = new JSDOM('<!doctype html><html><body><div id="root"></div></div></body></html>',
    {url: 'https://testhost.org'});
  global.window = jsdom.window;
  global.window.scrollTo = () => {};
  global.document = window.document;
  global.navigator = window.navigator;
  const localStorageMock = {
    setItem: () => {},
    getItem: () => {},
    removeItem: () => {}
  };
  global.localStorage = localStorageMock;
  return global.document.getElementById('root');
}
