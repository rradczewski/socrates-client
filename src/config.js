// @flow

import type {Config} from './ConfigType';

const config: Config = {
  environment: 'dev',
  jwtSecret: '$lsRTf!gksTRcDWs', // jwt key to work locally.
  siteUrl: 'http://localhost:3000',
  serverBackend: '/server/api/v1'
};

export default config;
