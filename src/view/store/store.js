// @flow
import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {createLogger} from 'redux-logger';
import createBrowserHistory from 'history/createBrowserHistory';
import createMemoryHistory from 'history/createMemoryHistory';
import {routerMiddleware, routerReducer} from 'react-router-redux';
import createTestLogger from '../../test/logger';



export class HistoryFactory {
  static createHistory() {
    return createBrowserHistory({basename: '/'});
  }

  static createTestHistory() {
    return createMemoryHistory();
  }
}

export default class StoreFactory {
  static _createStore(reducers: any, rootSaga: any, history: any, logger: any) {
    const sagaMiddleware = createSagaMiddleware();

    const middlewares = applyMiddleware(
      sagaMiddleware,
      routerMiddleware(history),
      logger
    );
    const allReducers = Object.assign(reducers, {router: routerReducer});
    const store = createStore(
      combineReducers(allReducers),
      compose(middlewares)
    );

    sagaMiddleware.run(rootSaga);

    return store;
  }

  static createStore(reducers: any, rootSaga: any, history: any) {
    return this._createStore(reducers, rootSaga, history, createLogger({collapsed: true}));
  }

  static createTestStore(reducers: any, rootSaga: any, history: any) {
    return this._createStore(reducers, rootSaga, history, createTestLogger());
  }


}
