import {NewsletterSignUpContainer} from './NewsletterSignUpContainer';
import {mount, shallow} from 'enzyme';
import React from 'react';
import {REMOVE_SUCCEEDED} from '../../newsletter-signout/NewsletterSignOutConstants';

const defaultProps = {
  message: '',
  signUp: () => {}
};
const _mount = (props) => {
  const finalProps = {...defaultProps, ...props};
  return mount(<NewsletterSignUpContainer {...finalProps}/>);
};

describe('NewsletterSignUpContainer', () => {
  let mockSignUp, newsletter;

  beforeEach(() => {
    mockSignUp = jest.fn();
    newsletter = _mount({signUp: mockSignUp});
  });

  afterEach(() => {
    newsletter.unmount();
  });

  function setInputValue(selector, value) {
    const wrapper = newsletter.find(selector);
    const node = wrapper.instance();
    node.value = value;
    wrapper.simulate('change', {target: {value}});
    newsletter.update();
  }

  function fillForm(name, email) {
    setInputValue('#newsletter-name', name);
    setInputValue('#newsletter-email', email);
  }

  function fillFormActivateCheckboxAndClickButton(name, email) {
    fillForm(name, email);
    const wrapper = newsletter.find('#newsletter-dataPrivacy');
    const node = wrapper.instance();
    node.checked = true;
    wrapper.simulate('change', {target: {value: 'dataPrivacyConfirmed'}});
    newsletter.update();
    newsletter.find('#newsletter-form-button').simulate('click');
  }

  it('empty name is invalid', () => {
    expect(newsletter.find('#newsletter-name').instance().className).toContain('is-invalid');
  });
  it('filled name is valid', () => {
    fillForm('name', '');
    expect(newsletter.find('#newsletter-name').instance().className).toContain('is-valid');
  });
  it('empty email is invalid', () => {
    expect(newsletter.find('#newsletter-email').instance().className).toContain('is-invalid');
  });
  it('filled well formatted email is valid', () => {
    fillForm('name', 'valid@example.com');
    expect(newsletter.find('#newsletter-email').instance().className).toContain('is-valid');
  });
  it('filled with invalid formatted email is invalid', () => {
    fillForm('name', 'invalidExample.com');
    expect(newsletter.find('#newsletter-email').instance().className).toContain('is-invalid');
  });
  it('should dispatch SignUp command when form is submitted', () => {
    fillFormActivateCheckboxAndClickButton('name', 'valid@example.com');
    expect(mockSignUp).toHaveBeenCalledTimes(1);
  });

  describe('when receiving props', () => {
    it('should render with message', () => {
      newsletter =
        shallow(<NewsletterSignUpContainer message={REMOVE_SUCCEEDED}/>);
      expect(newsletter).toMatchSnapshot();
    });

  });
});
