import React from 'react';
import NewsletterSignUpForm from './NewsletterSignUpForm';
import {shallow} from 'enzyme';

describe('(Component) NewsletterSignUpForm', () => {
  const wrapper = shallow(<NewsletterSignUpForm name="" hasValidName="false" email="" hasValidEmail="false"
    message="" onNameChange="() => {}" onEmailChange="() => {}" onSubmit="() => {}" isDisabled="true"/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
