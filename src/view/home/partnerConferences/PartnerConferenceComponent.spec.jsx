import React from 'react';
import {shallow} from 'enzyme/build/index';
import PartnerConferenceComponent from './PartnerConferenceComponent';

describe('PartnerConferenceComponent', () => {
  const conference = {
    name: 'SoCraTes Chile',
    description: '1 July 2016, Santiago de Chile',
    url: 'https://www.socrates-conference.cl/'
  };

  const wrapper = shallow(<PartnerConferenceComponent url={conference.url} name={conference.name}
    description={conference.description}/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
