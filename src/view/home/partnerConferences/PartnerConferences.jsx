// @flow

import React from 'react';
import PartnerConferenceComponent from './PartnerConferenceComponent';
import type {PartnerConference} from './partnerConferences.types';
import './conferences.css';

type Props = {
  conferences: PartnerConference[];
}

export default function PartnerConferences({conferences}: Props) {
  return <div id="conferences" className="conferences">
    <div className="row">
      <div className="col-sm-12">
        <hr/>
        <h4>Partner Conferences</h4>
      </div>
    </div>
    <div className="row">
      {
        conferences.map(c => <div className="conference col-sm-6" key={c.name}>
          <PartnerConferenceComponent url={c.url} name={c.name} description={c.description}/>
        </div>)
      }
    </div>
  </div>;
}
