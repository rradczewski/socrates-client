// @flow

export type PartnerConference = {
  name: string,
  description: string,
  url: string,
}

