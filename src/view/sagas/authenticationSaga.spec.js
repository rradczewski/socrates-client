// @flow
import axios from 'axios';
import AuthenticationEvent from '../events/authenticationEvents';
import SagaTester from 'redux-saga-tester';
import rootReducer from '../reducers/rootReducer';
import rootSaga from '../sagas/rootSaga';
import {logout} from '../commands/authenticationCommand';
import * as jwtStorage from '../services/jwtStorage';

describe('LogoutSaga', () => {
  let sagaTester: SagaTester;
  let setTokenSpy;
  let removeTokenSpy;
  const INITIAL_STATE = {
    placeHolder: {},
    authentication: {
      token: '',
      userName: 'Guest',
      isAdministrator: false,
      hasFinished: false
    }
  };

  beforeEach(async () => {
    setTokenSpy = jest.spyOn(jwtStorage, 'setToken');
    removeTokenSpy = jest.spyOn(jwtStorage, 'removeToken');

    sagaTester = new SagaTester({INITIAL_STATE, reducers: rootReducer});
    sagaTester.start(rootSaga);
    sagaTester.dispatch(logout());
    await sagaTester.waitFor(AuthenticationEvent.LOGOUT_SUCCESS);
  });

  afterEach(() => {
    sagaTester.reset();
    setTokenSpy.mockRestore();
    removeTokenSpy.mockRestore();
  });

  it('should remove jwt token from axios headers', () => {
    expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
  });

  it('should remove jwt token from store', () => {
    expect(removeTokenSpy).toHaveBeenCalled();
    expect(setTokenSpy).not.toHaveBeenCalled();
  });

  it('should dispatch LogoutSuccess event', () => {
    expect(sagaTester.getCalledActions()).toContainEqual({type: AuthenticationEvent.LOGOUT_SUCCESS});
  });
});
