//@flow

import api from '../requests/api';
import {call, put} from 'redux-saga/effects';
import {addFailed, addSucceeded, roomTypesReceived} from '../events/applicationEvents';
import {fatalError} from '../events/errorEvents';
import type {ApplicationData} from '../requests/api';

export function* fetchRoomTypesSaga(): Iterable<any> {
  try {
    const roomTypeOptions = yield call(api.getRoomTypeOptions);
    if (roomTypeOptions) {
      yield put(roomTypesReceived(roomTypeOptions));
    } else {
      yield put(fatalError('Room types could not be retrieved.'));
    }
  } catch (_) {
    yield put(fatalError('Service error while retrieving room types.'));
  }
}

export function* addApplicationSaga(command: ApplicationData): Iterable<any> {
  try {
    yield call(api.addApplication, command);
    yield put(addSucceeded());
  } catch (e) {
    yield put(addFailed(e.response && e.response.data ? e.response.data : 'FAIL'));
  }
}