// @flow
/* eslint-disable max-nested-callbacks */
import axios from 'axios';
import rootReducer from '../reducers/rootReducer';
import SagaTester from 'redux-saga-tester';
import rootSaga from './rootSaga';
import {addApplication, fetchRoomTypes} from '../commands/applicationCommand';
import ApplicationEvents from '../events/applicationEvents';
import {messages, messageTypes} from '../application/form/applicationMessages';

jest.mock('axios');

describe('ApplicationSaga', () => {
  let tester;

  beforeEach(() => {
    tester = new SagaTester({
      initialState: {
        application: {message: '', messageType: '', roomTypeOptions: []}
      },
      reducers: rootReducer
    });
    tester.start(rootSaga);
  });

  afterEach(() => {
    tester.reset();
  });

  describe('when application is successful', () => {
    const inputParams = {
      nickname: 'test',
      email: 'test@test.de',
      roomTypes: ['single', 'double'],
      diversity: 'yes'
    };

    beforeEach(async () => {
      axios.post.mockImplementation(() => Promise.resolve(true));
      const {nickname, email, roomTypes, diversity} = inputParams;
      tester.dispatch(addApplication(nickname, email, roomTypes, diversity));
      await tester.waitFor(ApplicationEvents.ADD_SUCCEEDED);
    });

    it('should set ADD_SUCCEEDED message', () => {
      const applicationState = tester.store.getState().application;
      expect(applicationState.message).toEqual(messages.SUCCESS);
      expect(applicationState.messageType).toEqual(messageTypes.SUCCESS);
    });

    it('should call /applications api', () => {
      const [url, params] = axios.post.mock.calls[0];
      expect(url).toEqual('/server/api/v1/applications');
      expect(params).toMatchObject(inputParams);
    });

    afterEach(() => axios.post.mockReset());
  });

  describe('when application fails', () => {
    const inputParams = {
      nickname: 'test',
      email: 'test@test.de',
      roomTypes: ['single', 'double'],
      diversity: 'yes'
    };

    describe('because an application already exists', () => {
      beforeEach(async () => {
        axios.post.mockImplementation(() => Promise.reject({response: {status: 400, data: 'ALREADY_APPLIED'}}));
        const {nickname, email, roomTypes, diversity} = inputParams;
        tester.dispatch(addApplication(nickname, email, roomTypes, diversity));
        await tester.waitFor(ApplicationEvents.ADD_FAILED);
      });

      it('should set ALREADY_APPLIED message', () => {
        const applicationState = tester.store.getState().application;
        expect(applicationState.message).toEqual(messages.ALREADY_APPLIED);
        expect(applicationState.messageType).toEqual(messageTypes.ALREADY_APPLIED);
      });

      it('should call /applications api', () => {
        const [url, params] = axios.post.mock.calls[0];
        expect(url).toEqual('/server/api/v1/applications');
        expect(params).toMatchObject(inputParams);
      });

    });

    describe('because there was a server error', () => {
      beforeEach(async () => {
        axios.post.mockImplementation(() => Promise.reject({response: {status: 500}}));
        const {nickname, email, roomTypes, diversity} = inputParams;
        tester.dispatch(addApplication(nickname, email, roomTypes, diversity));
        await tester.waitFor(ApplicationEvents.ADD_FAILED);
      });

      it('should set FAIL message', () => {
        const applicationState = tester.store.getState().application;
        expect(applicationState.message).toEqual(messages.FAIL);
        expect(applicationState.messageType).toEqual(messageTypes.FAIL);
      });

      it('should call /applications api', () => {
        const [url, params] = axios.post.mock.calls[0];
        expect(url).toEqual('/server/api/v1/applications');
        expect(params).toMatchObject(inputParams);
      });

    });

    afterEach(() => axios.post.mockReset());
  });

  describe('when fetching room types is successful', () => {
    const ROOM_TYPES = [
      {value: 'single', label: 'Single', prices: ['268€']},
      {value: 'junior', label: 'Junior (exclusively)', prices: ['234€']}
    ];

    beforeEach(async () => {
      axios.get.mockImplementation(() => Promise.resolve({status: 200, data:ROOM_TYPES}));
      tester.dispatch(fetchRoomTypes());
      await tester.waitFor(ApplicationEvents.ROOM_TYPES_RECEIVED);
    });

    it('should set roomTypes in state', () => {
      const applicationState = tester.store.getState().application;
      expect(applicationState.roomTypeOptions).toEqual(ROOM_TYPES);
    });

    it('should call /applications api', () => {
      const [url] = axios.get.mock.calls[0];
      expect(url).toEqual('/server/api/v1/applications/options');
    });

    afterEach(() => axios.get.mockReset());
  });

  describe('when fetching room types fails', () => {
    describe('because of a server error', () => {
      beforeEach(()=>axios.get.mockImplementation(() => Promise.reject(new Error())));
      it('should dispatch FATAL_ERROR', async () => {
        tester.dispatch(fetchRoomTypes());
        await tester.waitFor('FATAL_ERROR');
      });
    });
    describe('because there were no room options', () => {
      beforeEach(()=>axios.get.mockImplementation(() => Promise.resolve({status:200})));
      it('should dispatch FATAL_ERROR', async () => {
        tester.dispatch(fetchRoomTypes());
        await tester.waitFor('FATAL_ERROR');
      });
    });

    afterEach(() => axios.get.mockReset());
  });
});