// @flow
import axios from 'axios';
import rootReducer from '../reducers/rootReducer';
import SagaTester from 'redux-saga-tester';
import rootSaga from './rootSaga';
import {ADD_FAILED, ADD_SUCCEEDED, EMPTY} from '../home/newsletter/NewsletterSignUpConstants';
import {signUp, signOut} from '../commands/newsletterCommand';
import NewsletterEvents from '../events/newsletterEvents';
import {REMOVE_FAILED, REMOVE_SUCCEEDED} from '../newsletter-signout/NewsletterSignOutConstants';

jest.mock('axios');

describe('NewsletterSaga', () => {
  let tester;

  beforeEach(() => {
    tester = new SagaTester({initialState: {newsletter: {message: EMPTY}}, reducers: rootReducer});
    tester.start(rootSaga);
  });
  afterEach(() => {
    tester.reset();
  });

  describe('when signUp is successful', () => {
    beforeEach(async () => {
      axios.post.mockImplementation(() => Promise.resolve(true));
      tester.dispatch(signUp('test', 'test@test.de'));
      await tester.waitFor(NewsletterEvents.ADD_SUCCEEDED);
    });

    it('should set ADD_SUCCEEDED message', () => {
      expect(tester.store.getState().newsletter.message).toEqual(ADD_SUCCEEDED);
    });

    it('should call /interested-people api', () => {
      const [url, params] = axios.post.mock.calls[0];
      expect(url).toEqual('/server/api/v1/interested-people');
      expect(params).toEqual({name: 'test', email: 'test@test.de'});
    });

    afterEach(() => axios.post.mockReset());
  });

  describe('when signUp is not successful', () => {
    beforeEach(async () => {
      axios.post.mockImplementation(() => Promise.resolve(false));
      tester.dispatch(signUp('test', 'test@test.de'));
      await tester.waitFor(NewsletterEvents.ADD_FAILED);
    });

    it('should set ADD_FAILED message', () => {
      expect(tester.store.getState().newsletter.message).toEqual(ADD_FAILED);
    });

    it('should call /interested-people api', () => {
      const [url, params] = axios.post.mock.calls[0];
      expect(url).toEqual('/server/api/v1/interested-people');
      expect(params).toEqual({name: 'test', email: 'test@test.de'});
    });

    afterEach(() => axios.post.mockReset());
  });

  describe('when signUp results in an error', () => {
    beforeEach(async () => {
      axios.post.mockImplementation(() => Promise.reject());
      tester.dispatch(signUp('test', 'test@test.de'));
      await tester.waitFor(NewsletterEvents.ADD_FAILED);
    });

    it('should set ADD_FAILED message', () => {
      expect(tester.store.getState().newsletter.message).toEqual(ADD_FAILED);
    });

    it('should call /interested-people api', () => {
      const [url, params] = axios.post.mock.calls[0];
      expect(url).toEqual('/server/api/v1/interested-people');
      expect(params).toEqual({name: 'test', email: 'test@test.de'});
    });

    afterEach(() => axios.post.mockReset());
  });

  describe('when signOut is successful', () => {
    beforeEach(async () => {
      axios.delete.mockImplementation(() => Promise.resolve(true));
      tester.dispatch(signOut('test@test.de'));
      await tester.waitFor(NewsletterEvents.REMOVE_SUCCEEDED);
    });

    it('should set REMOVE_SUCCEEDED message', () => {
      expect(tester.store.getState().newsletter.message).toEqual(REMOVE_SUCCEEDED);
    });

    it('should call /interested-people api', () => {
      const [url] = axios.delete.mock.calls[0];
      expect(url).toEqual('/server/api/v1/interested-people/test@test.de');
    });

    afterEach(() => axios.delete.mockReset());
  });

  describe('when signOut is not successful', () => {
    beforeEach(async () => {
      axios.delete.mockImplementation(() => Promise.resolve(false));
      tester.dispatch(signOut('test@test.de'));
      await tester.waitFor(NewsletterEvents.REMOVE_FAILED);
    });

    it('should set REMOVE_FAILED message', () => {
      expect(tester.store.getState().newsletter.message).toEqual(REMOVE_FAILED);
    });

    it('should call /interested-people api', () => {
      const [url] = axios.delete.mock.calls[0];
      expect(url).toEqual('/server/api/v1/interested-people/test@test.de');
    });

    afterEach(() => axios.delete.mockReset());
  });

  describe('when signOut results in an error', () => {
    beforeEach(async () => {
      axios.delete.mockImplementation(() => Promise.reject());
      tester.dispatch(signOut('test@test.de'));
      await tester.waitFor(NewsletterEvents.REMOVE_FAILED);
    });

    it('should set REMOVE_FAILED message', () => {
      expect(tester.store.getState().newsletter.message).toEqual(REMOVE_FAILED);
    });

    it('should call /interested-people api', () => {
      const [url] = axios.delete.mock.calls[0];
      expect(url).toEqual('/server/api/v1/interested-people/test@test.de');
    });

    afterEach(() => axios.delete.mockReset());
  });

});