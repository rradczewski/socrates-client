// @flow
import {all, put, takeEvery} from 'redux-saga/effects';
import RoutingCommand from '../commands/routingCommand';
import {push} from 'react-router-redux';
import AuthenticationCommand from '../commands/authenticationCommand';
import {logoutSaga} from './authenticationSaga';
import NewsletterCommand from '../commands/newsletterCommand';
import {confirmSaga, signOutSaga, signUpSaga} from './newsletterSaga';
import ApplicationCommand from '../commands/applicationCommand';
import {addApplicationSaga, fetchRoomTypesSaga} from './applicationSaga';

function* routingToSaga(action: any) {
  yield put(push(action.url));
}

function* rootSaga(): any {
  yield all([
    takeEvery(RoutingCommand.ROUTE_TO, routingToSaga),
    takeEvery(ApplicationCommand.ADD_APPLICATION, addApplicationSaga),
    takeEvery(ApplicationCommand.FETCH_ROOM_TYPES, fetchRoomTypesSaga),
    takeEvery(AuthenticationCommand.LOGOUT, logoutSaga),
    takeEvery(NewsletterCommand.SIGN_UP, signUpSaga),
    takeEvery(NewsletterCommand.SIGN_OUT, signOutSaga),
    takeEvery(NewsletterCommand.CONFIRM, confirmSaga)
  ]);
}

export default rootSaga;
