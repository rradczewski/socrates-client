// @flow

import {
  call,
  put
} from 'redux-saga/effects';
import {
  loginError,
  loginStarted,
  loginSuccess,
  logoutSuccess
} from '../events/authenticationEvents';
import api from '../requests/api';
import * as jwt from 'jsonwebtoken';
import {routeTo} from '../commands/routingCommand';
import config from '../../config';
import * as sjcl from 'sjcl';
import {setToken, removeToken} from '../services/jwtStorage';
import {setAuthorizationHeader, removeAuthorizationHeader} from '../requests/clientConfig';

export const jsonWebTokenSecret = config.jwtSecret;
export type LoginCommandData = {
  email: string,
  password: string,
  comesFrom: string
}

export function* loginSaga(action: LoginCommandData): Iterable<any> {
  yield put(loginStarted());
  try {
    const sha256 = sjcl.hash.sha256.hash(action.password);
    const hash = sjcl.codec.hex.fromBits(sha256);
    const token: ?string = yield call(api.login, action.email, hash);
    if (token && token.trim().length > 0) {
      const userData = yield call(jwt.verify, token, jsonWebTokenSecret);
      if (userData) { // only for flow: userData is never not truthy, otherwise there would be an error
        setAuthorizationHeader(token);
        setToken(token);
        yield put(loginSuccess(token, userData));
        yield put(routeTo(action.comesFrom));
      }
    } else {
      removeSession();
      yield put(loginError());
    }
  } catch (error) {
    removeSession();
    yield put(loginError());
  }
}

export function* logoutSaga(): Iterable<any> {
  removeSession();
  yield put(logoutSuccess());
}

const removeSession = () => {
  removeAuthorizationHeader();
  removeToken();
};
