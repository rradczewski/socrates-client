import React from 'react';
import NewsletterConfirmForm from './NewsletterConfirmForm';
import {shallow} from 'enzyme';

describe('(Component) NewsletterConfirmForm', () => {
  const wrapper = shallow(<NewsletterConfirmForm email="" hasValidEmail="false"
    message="" onNameChange="() => {}" onEmailChange="() => {}" onSubmit="() => {}" isDisabled="true"/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
