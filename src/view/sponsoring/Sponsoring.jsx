// @flow

import React from 'react';

export default function Sponsoring() {
  const the1Spot = 'mailto:sponsoring@socrates-conference.de?subject=I%20am%20interested%20in%20the%20One%20Package';
  const the2Spots = 'mailto:sponsoring@socrates-conference.de?subject=I%20am%20interested%20in%20the%20Two%20Package';
  const the3Spots = 'mailto:sponsoring@socrates-conference.de?subject=I%20am%20interested%20in%20the%20Three%20Package';
  const the4Spots = 'mailto:sponsoring@socrates-conference.de?subject=I%20am%20interested%20in%20the%20Four%20Package';
  const the5Spots = 'mailto:sponsoring@socrates-conference.de?subject=I%20am%20interested%20in%20the%20Five%20Package';
  const workshop = 'mailto:sponsoring@socrates-conference.de?subject=I%20am%20interested%20in%20sponsoring' +
    '%20a%20SoCraTes%20sunday%20workshop';
  const sponsoring = 'mailto:sponsoring@socrates-conference.de?subject=I%20would%20like%20information%20about%20' +
    'sponsoring';
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-12">
          <div className="page-header">
            <h1>Sponsoring SoCraTes</h1>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col"><p>Thank you for your interest in making SoCraTes 2018 happen!</p>
          <p>We invite you to sponsor the conference and help make this a brilliant event. Your financial support
            makes a huge difference to the event and helps make sure it will be affordable for our attendees.</p>
          <div className="card card-body bg-light"><h3>About the event</h3>
            <p>The seventh annual International Conference for Software Craft and Testing will take place
              from 23 - 26 August 2018 in Soltau near Hamburg, Germany.</p>
            <p>This year, up to 220 people can participate &ndash; after our successful upscaling to 160
              participants in 2015.</p>
            <p>The event will be non-commercial, community-driven and low-price. A two-day Open Space will be
              followed by a Coderetreat and other workshops. SoCraTes is an international non-profit conference for
              craftspeople by craftspeople.</p>
            <p>Contact: <a href={sponsoring}><i className="fas fa-envelope"></i>
              <span> sponsoring@socrates-conference.de</span></a></p>
          </div>
          <h2>Our Offer</h2>
          <p>Sponsoring SoCraTes means supporting the conference, as well as supporting the community of Software
            Crafters and Testers. It is not something you buy - it is something you give.</p>
          <p>However, in return for the support you give, we reward you with preferred treatment regarding the lottery:
            <i>Your</i> candidates <i>will</i> receive a seat without going through the random selection process.
            The number of candidates this applies to is proportional to the size of your contribution; the maximum
            number is 5.</p>
          <p>In order for this arrangement to work, candidate names have to be submitted <strong>before the day of the
            lottery</strong>, otherwise the seats will be automatically freed for other applicants.</p>
          <p>If you sign up after the lottery has taken place, you will have to wait until tickets are canceled,
            before new candidates can be approved. You will still receive preferred treatment (no lottery), but in
            order for a seat to be given away, it has to be available first. There can also never be a guarantee that
            all the sponsored slots will become available after completion of the lottery, for obvious reasons.</p>

          <p>For sponsorings of 750 &euro; or more we offer the following:</p>
          <ul>
            <li><b>Your logo</b> on our website</li>
            <li>Our logo for your <b>marketing activities</b></li>
            <li>You will be <b>mentioned</b> during the opening and closing sessions of the conference</li>
            <li>You will be mentioned in SoCraTes&#39; <b>official social media</b> communications and <b>
              official mailings</b></li>
          </ul>
          <p>Depending on the amount you choose to invest, you will also get</p>
          <ul>
            <li>A number of reserved <b>participant slots</b>
              , you do not need to take part in the lottery (you&#39;ll still need to pay for the hotel accommodation)
            </li>
          </ul>
          <p>Best of all, by the virtue of sending your employees to the conference, you&#39;ll have the opportunity
            to come in contact with highly motivated and skilled craftspeople.
          </p>
          <div className="panel panel-default">
            <div className="panel-heading"><h4>Sponsored Participants</h4></div>
            <table className="table table-condensed table-small">
              <thead>
                <tr>
                  <th></th>
                  <th>One (750 &euro;)</th>
                  <th>Two (1500 &euro;)</th>
                  <th>Three (2250 &euro;)</th>
                  <th>Four (3000 &euro;)</th>
                  <th>Five (3750 &euro;)</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>Slots</th>
                  <td>1</td>
                  <td>2</td>
                  <td>3</td>
                  <td>4</td>
                  <td>5</td>
                </tr>
                <tr>
                  <th>Mailings</th>
                  <td>No</td>
                  <td>Yes</td>
                  <td>Yes</td>
                  <td>Yes</td>
                  <td>Yes</td>
                </tr>
                <tr className="no-print">
                  <th></th>
                  <td>
                    <a href={the1Spot}><i className="fas fa-envelope"></i><span> Contact us</span></a>
                  </td>
                  <td>
                    <a href={the2Spots}><i className="fas fa-envelope"></i><span> Contact us</span></a>
                  </td>
                  <td>
                    <a href={the3Spots}><i className="fas fa-envelope"></i><span> Contact us</span></a>
                  </td>
                  <td>
                    <a href={the4Spots}><i className="fas fa-envelope"></i><span> Contact us</span></a>
                  </td>
                  <td>
                    <a href={the5Spots}><i className="fas fa-envelope"></i><span> Contact us</span></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <p>
            <small>
              In addition to the above packages, you can also <a
                href={workshop}><i className="fas fa-envelope"></i><span> sponsor </span>
              </a>one of the dedicated workshops or the Coderetreat that will take place on Sunday. This is an
              individual arrangement and both the price and the form of your presentation can be discussed.
            </small>
          </p>
        </div>
      </div>
    </div>
  );
}

