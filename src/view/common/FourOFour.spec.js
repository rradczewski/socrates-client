import React from 'react';
import {shallow} from 'enzyme';
import FourOFour from './FourOFour';

describe('(Component) FourOFour', () => {
  const wrapper = shallow(<FourOFour />);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
