// @flow

import React from 'react';
import Sponsors from './Sponsors';
import './sponsoring.css';
import {Link} from 'react-router-dom';
import shuffleArray from '../../../utils/shuffleArray';
import sponsors from './sponsorInfo';
import mediaPartners from './mediaPartnersInfo';

export default function Sponsoring() {
  return <div id="sponsoring">
    <Sponsors title="Our Sponsors" showEmpty={true} sponsors={shuffleArray(sponsors)}/>
    <Sponsors title="Our Media Partners" sponsors={shuffleArray(mediaPartners)}/>
    <Link to="/sponsoring" className="btn btn-info pull-right"><span className="far fa-thumbs-up"/> Sponsor us </Link>
  </div>;
}

