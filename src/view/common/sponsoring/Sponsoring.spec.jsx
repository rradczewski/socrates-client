import React from 'react';
import Sponsoring from './Sponsoring';
import {shallow} from 'enzyme';
import StoreFactory, {HistoryFactory} from '../../store/store';
import {Provider} from 'react-redux';

function* emptySaga(){
}

describe('(Component) Sponsoring', () => {
  const history = HistoryFactory.createTestHistory();
  const store = StoreFactory.createStore({}, emptySaga, history);

  const wrapper = shallow(<Provider store={store}><Sponsoring/></Provider>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});