// @flow

import React from 'react';

export default function FourOFour() {
  return (
    <div>
      <h1>404</h1>
      <h2>Ops, sorry we cannot find that page!</h2>
    </div>
  );
}
