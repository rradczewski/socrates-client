// @flow

import React from 'react';
import {Link} from 'react-router-dom';
import './footer.css';

export default function Footer() {
  const twitterUrl = 'https://twitter.com/intent/tweet?text=SoCraTes+2018+will+take+place+from+23+to+26+August.' +
    '+More+at&amp;url=http%3A%2F%2Fsocrates-conference.de%2F&amp;hashtags=socrates_18&amp;via=SoCraTes_Conf';

  return (
    <div id="footer" className="container">
      <footer>
        <div className="row">
          <hr/>
          <div className="col-sm-12">
            <div className="btn-group float-right">
              <Link className="btn btn-info" to="/imprint" title="Impressum">
                <span className="fas fa-info"></span>
                <span className="d-none d-xl-inline"> Legal Notice</span>
              </Link>
              <a className="btn btn-info" href="mailto:info@socrates-conference.de" title="E-Mail">
                <span className="fas fa-envelope"></span>
                <span className="d-none d-xl-inline"> E-mail</span>
              </a>
              <a className="btn btn-info" href={twitterUrl} title="Tweet">
                <span className="fab fa-twitter"></span>
                <span className="d-none d-xl-inline"> Tweet</span>
              </a>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}
