// @flow
import React from 'react';
import {connect} from 'react-redux';
import NavigationBrand from './NavigationBrand';
import NavigationToggler from './NavigationToggler';
import DropdownLink from './DropdownLink';
import DropdownItem from './DropdownItem';
import DropdownToggler from './DropdownToggler';
import {withRouter} from 'react-router';
import type {AuthenticationState} from '../../reducers/authenticationReducer';
import {logout} from '../../commands/authenticationCommand';
import config from '../../../config';
import NavigationLink from './NavigationLink';
import NavigationApplicationLink from './NavigationApplicationLink';

type Props = {
  authentication: AuthenticationState,
  logout: () => void
}

function Navigation(props: Props) {
  const isLoggedIn = props.authentication.token.trim() !== '';
  const collapsiblePanelId = 'navbarSupportedContent';
  const userDropdown = 'navbarDropdownMenuLink';
  const loginUrl = `${config.siteUrl}/login`;
  const profileUrl = `${config.siteUrl}/profile`;
  return (
    <div id="navigation">
      <nav className="navbar navbar-expand-md navbar-dark bg-dark">
        <NavigationBrand/>
        <NavigationToggler target={collapsiblePanelId}/>
        <div className="collapse navbar-collapse" id={collapsiblePanelId}>
          <ul className="navbar-nav mr-auto">
            <NavigationLink url="/format" title="Format" iconClass="fas fa-calendar-check"/>
            <NavigationLink url="/location" title="Location" iconClass="fas fa-globe"/>
            <NavigationLink url="/values" title="Values" iconClass="fas fa-gift"/>
            <NavigationLink url="/accessibility" title="Accessibility" iconClass="fab fa-accessible-icon"/>
            <NavigationLink url="/faq" title="FAQ" iconClass="fas fa-question-circle"/>
            <NavigationLink url="/history" title="History" iconClass="fas fa-history"/>
            <NavigationLink url="/imprint" title="Imprint" iconClass="fas fa-paragraph"/>
            <NavigationLink url="/privacy-policy" title="Privacy policy" iconClass="fas fa-user-secret"/>
            <NavigationApplicationLink visible={!isLoggedIn}/>
          </ul>
          <div className="navbar-nav navbar-item mr-3 dropdown">
            <DropdownToggler
              target={userDropdown}
              url="#navigation"
              title={props.authentication.userName}
              iconClass="fas fa-user"
            />
            <div className="dropdown-menu bg-dark dropdown-menu-right" aria-labelledby={userDropdown}>
              <DropdownLink visible={!isLoggedIn} url={loginUrl} title="Login" iconClass="fas fa-sign-in-alt"/>
              <DropdownLink visible={isLoggedIn} url={profileUrl} title="Profile" iconClass="fas fa-users"/>
              <DropdownItem visible={isLoggedIn} url="#navigation" title="Logout" iconClass="fas fa-sign-out-alt"
                onClick={props.logout}/>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}

const mapStateToProps = state => {
  return {authentication: {...state.authentication}};
};

const mapDispatchToProps = {
  logout
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navigation));
