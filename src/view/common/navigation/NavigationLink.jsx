// @flow
import {NavLink} from 'react-router-dom';
import React from 'react';

type NavigationLinkProps = {
  url: string,
  title: string,
  iconClass: string
}

export default function NavigationLink(props: NavigationLinkProps) {
  return (
    <li className="nav-item">
      <NavLink
        className="nav-link"
        to={props.url}
        title={props.title}
        data-toggle="collapse"
        data-target=".navbar-collapse.show">
        <span className={props.iconClass}></span>
        <span className="d-md-none d-lg-none d-xl-inline"> {props.title}</span>
      </NavLink>
    </li>
  );
}

