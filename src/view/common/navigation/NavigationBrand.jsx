// @flow

import {Link} from 'react-router-dom';
import React from 'react';
import bradLogo from '../../../assets/img/socrates_no_text_40.png';

export default function NavigationBrand() {
  return (
    <Link className="navbar-brand" to="/home" title="Home">
      <span>
        <img src={bradLogo} alt="logo"/>
      </span>
      <span> SoCraTes 2018</span>
    </Link>
  );
}
