// @flow
import React from 'react';

type DropdownTogglerProps = {
  target: string,
  url: string,
  title: string,
  iconClass: string,
}

export default function DropdownToggler(props: DropdownTogglerProps) {
  return (
    <a
      className="nav-link dropdown-toggle"
      href={props.url}
      id={props.target}
      data-toggle="dropdown"
      aria-haspopup="true"
      aria-expanded="false"
    >
      <span className={props.iconClass}></span>
      <span className="d-md-none d-lg-none d-xl-inline"> {props.title}</span>
    </a>
  );
}
