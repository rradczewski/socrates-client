// @flow
import {Link} from 'react-router-dom';
import React from 'react';

type Props = {visible: boolean};
export default function NavigationApplicationLink({visible}: Props) {
  return (
    <li className={'nav-item' + (visible ? ' d-block' : ' d-none')}>
      <Link className="btn btn-primary pull-right btn-application" to="/application">Apply for a ticket</Link>
    </li>
  );
}

