const KEY = 'token';

export const setToken = token =>
  localStorage.setItem(KEY, token);

export const removeToken = () =>
  localStorage.removeItem(KEY);

export const getToken = () =>
  localStorage.getItem(KEY);
