/* eslint-disable */
// @flow

import React from 'react';
import {Link} from 'react-router-dom';
import '../application.css';
import Accordion from './Accordion';
import splitArrayIntoTwoParts from '../../../utils/splitArrayIntoTwoParts';

const entries = [
  {
    title: 'How it works.',
    content:
      <div>
        <p>The tickets will be given out in a lottery.</p>
        <p>The registration opens on April 24, 2018. To avoid "rush hour" traffic, all registrations (except for
          guaranteed sponsor slots) will end up on the waiting list, initially.
          It will not matter, if you register on the first day, or on the last.
          On May 15, 2018 there will be a draw to distribute the slots to people on the waiting list.
        </p>
        <p>Please note that the draw will be (pseudo-) random, and we will not influence the results in any
          way.</p>
        <p>After the draw, you will be notified, if you are in.</p>
        <p>If you are not, you stay on the waiting list (as was previous practice). But don’t give up just yet:
          Cancellations and other issues always cause some slots to become available again, and we will draw those
          away at regular intervals.
        </p>
      </div>
  },
  {
    title: 'Room Options',
    content:
      <div>
        <p>We offer three kinds of rooms. Please choose the room you'd like and tell us for how long you will be
          our guest.
          All rooms need to be booked from Thursday evening. There will be no exceptions.
        </p>
        <ul>
          <li>Single bed room</li>
          <li>Double bed room, can only be shared</li>
          <li>Junior double bed room (one bed, 160 cm x 200 cm), can be shared or occupied exclusively</li>
        </ul>
        <p>If you stay at least until Sunday evening, we assume that you want to participate in the Coderetreat or
          in one of the workshops on Sunday.</p>
        <p>All prices are per person and include:</p>
        <ul>
          <li>participation in the SoCraTes conference (including the Sunday Coderetreat or workshop, if you
            like)
          </li>
          <li>the hotel room</li>
          <li>meals, snacks and coffee</li>
        </ul>
        <p>The final prices depend on our sponsors and may be slightly lower than announced here.</p>
      </div>
  },
  {
    title: 'Room Choice',
    content:
      <div>
        <p>You can select as many room options  as you like.</p>
        <p>If you select more than one option, this means you are happy with any of the room types you selected.
          We will move you to one of the rooms, as they become available.
        </p>
        <p>Generally speaking: The more waiting list options you select, the better your chances are to get a spot,
          or to improve your selected room type.</p>
        <p>Please be aware that we will move you to the next available room, without checking back with you first.
          So please
          select only those options that you are happy with!
        </p>
      </div>
  },
  {
    title: 'Payment and Invoice',
    content:
      <div>
        <p>You will pay the full amount (depending on your room choice and length of stay, and on how much sponsoring
          money we can collect) to the venue on checkout, and you will receive an invoice from the hotel.</p>
        <p>The final amount will include food, group rooms and free non-alcoholic drinks, plus the hotel service fee
          for the conference - at most 202,50€, if you stay from Thursday until Sunday (all of which will be covered
          by sponsoring money, if possible) - as well as your hotel room (see pricing table below).</p>
        <p>To give you an example: The price for a single room for all 3 nights will be somewhere between roughly
          200€ and 400€, with a high likelihood of being closer to the lower end.</p>
        <table className="pricing">
          <thead>
            <tr>
              <th>Room type</th><th>Price p. person/night</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Single</td><td className="numeric">62,50&euro;</td>
            </tr>
            <tr>
            <td>Junior Double (exclusively)</td><td className="numeric">62,50&euro;</td>
            </tr>
            <tr>
            <td>Double (shared)</td><td className="numeric">47,95&euro;</td>
            </tr>
            <tr>
            <td>Junior Double (shared)</td><td className="numeric">45,45&euro;</td>
            </tr>
          </tbody>
        </table>
      </div>
  },
  {
    title: 'I\'m in, and now?',
    content:
      <div>
        <p>Some of you have booked a spot in a double shared, or junior shared room. Now is the time to specify
          your desired
          roommate. Please do so as soon as possible. You can check or set your roommate in your SoCraTes profile.
        </p>
        <p>All those who do not choose a roommate until the end of July 2018 will be assigned one. We will usually
          fill the
          spot with someone matching your own gender, unless you told us to do otherwise, and of course providing
          enough
          matching pairs exist. However the draw turns out, though, we want you to be sure that there is no way we
          will
          force you to share the room with someone you dislike or have doubts about - please contact us, if this
          should be
          the case, and we will try our best to find a mutually agreeable solution.
        </p>
        <p>Please verify the duration of your stay. If anything needs to be changed, send an email to
          registration@socrates-conference.de, along with your desired changes. You can check this data on the
          registration
          page, after logging in (https://www.socrates-conference.de/registration).
        </p>
        <p>To book until Sunday evening means participation in the workshops or code retreat - as sponsorship
          money is being
          used to make this possible, please be fair and do not unnecessarily specify the Sunday evening, unless
          you want
          to participate in these activities.
        </p>
        <p>Specify the correct size of your SoCraTes t-shirt. Check and change this setting, if necessary. If you
          don’t want
          a t-shirt, please select the “no shirt” option.
        </p>
        <p>Also check your home and billing addresses on the profile page. You may tell us about your food
          preferences/dietary
          restrictions, whether you have any special needs, or any other information you want to share with us.
        </p>
        <p>Please make sure that your profile fields are up to date, if this isn’t your first SoCraTes
          (roommate from last year, room type, etc).
        </p>
      </div>
  }
];

export default function ApplicationDescription() {
  const twoColumnEntries = splitArrayIntoTwoParts(entries);
  const accordionId = 'registration-accordion';
  return (
    <div>
      <div className="row">
        <div className="col-md-12">
          <h4 className="withTopMargin">
            Do you want to skip the lottery and get a secured spot for SoCraTes? Become a <Link
            to="/sponsoring">sponsor</Link>.
          </h4>
        </div>
      </div>
      <div className="row" id={accordionId}>
        <div className="col-md-6">
          <Accordion parentId={accordionId} entries={twoColumnEntries[0]}/>
        </div>
        <div className="col-md-6">
          <Accordion parentId={accordionId} entries={twoColumnEntries[1]}
                                 startIndex={twoColumnEntries[0].length}/>
        </div>
      </div>
    </div>
  );
}
