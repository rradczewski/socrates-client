export const messageTypes = {
  EMPTY: 'EMPTY',
  SUCCESS: 'SUCCESS',
  FAIL: 'FAIL',
  ALREADY_APPLIED: 'ALREADY_APPLIED'
};

export const messages = {
  EMPTY: '',
  SUCCESS: 'Thank you for caring about Software Craft. You are now taking part in the lottery for SoCraTes.',
  FAIL: 'Unfortunately the api is down, please try again later.',
  ALREADY_APPLIED: 'Registration failed. An application for this email already exists. If you want to change' +
' your data, please send an email to registration@socrates-conference.de instead.'
};
