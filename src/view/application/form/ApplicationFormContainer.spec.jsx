import React from 'react';
import {ApplicationFormContainer} from './ApplicationFormContainer';
import {mount, shallow} from 'enzyme';
import {messages, messageTypes} from './applicationMessages';

const ROOM_TYPES = [
  {value: 'single', label: 'Single', prices: ['268€']},
  {value: 'junior', label: 'Junior (exclusively)', prices: ['234€']}
];

const defaultProps = {
  addApplication: () => {},
  message: '',
  messageType: '',
  roomTypeOptions: ROOM_TYPES,
  fetchRoomTypes: () => {}
};
const _mount = props => {
  const finalProps = {...defaultProps, ...props};
  return mount(<ApplicationFormContainer {...finalProps}/>);
};

describe('(Component) ApplicationFormContainer', () => {
  describe('when mounted', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = shallow(<ApplicationFormContainer
        addApplication={() => {}}
        message=""
        messageType=""
        roomTypeOptions={ROOM_TYPES}
        fetchRoomTypes={() => {}}
      />);
    });

    it('renders without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });

  });

  describe('when inserting values', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = _mount();
    });

    describe('and nickname is empty', () => {
      beforeEach(() => wrapper.find('#nickname').simulate('change', {target: {value: ''}}));

      it('should render invalid', () => {
        expect(wrapper.find('#nickname').hasClass('is-invalid')).toBe(true);
      });
    });

    describe('and email is empty', () => {
      beforeEach(() => wrapper.find('#email').simulate('change', {target: {value: 'invalid'}}));

      it('should render invalid', () => {
        expect(wrapper.find('#email').hasClass('is-invalid')).toBe(true);
      });
    });

    describe('and form is incomplete', () => {
      it('should disable submit button', () => {
        expect(wrapper.find('#submit-application').prop('disabled')).toBeTruthy();
      });
    });

    describe('and form is complete', () => {
      beforeEach(() => {
        wrapper.find('#nickname').simulate('change', {target: {value: 'jan3 d0e'}});
        wrapper.find('#email').simulate('change', {target: {value: 'jane@example.com'}});
        wrapper.find('#junior').simulate('click', {target: {value: 'junior'}});
        wrapper.find('#single').simulate('click', {target: {value: 'single'}});
        wrapper.find('#yes').simulate('change', {target: {value: 'yes'}});
        wrapper.find('#application-dataPrivacy').simulate('change', {target: {value: 'dataPrivacyConfirmed'}});
      });

      it('should enable submit button', () => {
        expect(wrapper.find('#submit-application').prop('disabled')).toBeFalsy();
      });
    });
  });

  describe('when submitting form', () => {
    const nickname = 'jan3 d0e';
    const email = 'jane@example.com';
    const roomTypes = ['junior'];
    const diversity = 'yes';

    it('should dispatch addApplication command', done => {
      const wrapper = _mount({
        addApplication: (name, mail, rooms, div) => {
          expect(name).toEqual(nickname);
          expect(mail).toEqual(email);
          expect(rooms).toEqual(roomTypes);
          expect(div).toEqual(diversity);
          done();
        }
      });

      wrapper.find('#nickname').simulate('change', {target: {value: nickname}});
      wrapper.find('#email').simulate('change', {target: {value: email}});
      wrapper.find('#junior').simulate('click', {target: {value: roomTypes[0]}});
      wrapper.find('#single').simulate('click', {target: {value: roomTypes[1]}});
      wrapper.find('#single').simulate('click', {target: {value: roomTypes[1]}});
      wrapper.find('#yes').simulate('change', {target: {value: diversity}});
      wrapper.find('form').simulate('submit');
    });
  });

  describe('when message is returned from api', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = _mount({
        message: messages.SUCCESS,
        messageType: messageTypes.SUCCESS
      });
    });

    it('should display the message', () => {
      expect(wrapper.find('#application-message').text()).toEqual(messages.SUCCESS);
    });

    it('should hide the form', () => {
      expect(wrapper.find('form')).toHaveLength(0);
    });
  });
})
;
