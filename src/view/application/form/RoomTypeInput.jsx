// @flow

import React from 'react';
import PropTypes from 'prop-types';
import type {RoomTypeOptions} from './RoomTypeOptions';

type Props = {
  onClick: SyntheticInputEvent<HTMLInputElement> => void,
  options: RoomTypeOptions,
  selected: Array<string>
};

export function RoomTypeInput({onClick, options, selected}: Props) {
  const hasOptions = options.length > 0;
  return hasOptions && (
    <div className="form-group">
      <label htmlFor="roomType" className="col-form-label">Room Type</label>
      <div className="table-responsive">
        {options.map(({value, label}) => (
          <div key={value} className="radio-inline">
            <label> <input checked={selected.includes(value)} id={value} name="roomType"
              onClick={onClick} type="checkbox" value={value}/> <b>&nbsp;{label}</b> </label>
          </div>
        ))}
      </div>
    </div>
  );
}

RoomTypeInput.propTypes = {
  onClick: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired,
  selected: PropTypes.array.isRequired
};
