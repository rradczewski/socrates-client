// @flow

export type RoomTypeOption = {
  value: string, label: string, prices: Array<string>
}

export type RoomTypeOptions = Array<RoomTypeOption>
