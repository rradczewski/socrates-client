// @flow

import React from 'react';
import {connect} from 'react-redux';
import {messageTypes, messages} from './applicationMessages';
import isValidEmailFormat from '../../../utils/isValidEmailFormat';
import {ApplicationForm} from './ApplicationForm';
import type {RoomTypeOptions} from './RoomTypeOptions';
import {addApplication, fetchRoomTypes} from '../../commands/applicationCommand';

type Props = {
  addApplication: (nickname: string, email: string, roomTypes: string[], diversity: string) => void,
  message: string,
  messageType: string,
  roomTypeOptions: RoomTypeOptions,
  fetchRoomTypes: () => void
};

type State = {
  diversitySelected: string,
  email: string,
  hasSelectedRoomType: boolean,
  hasValidEmail: boolean,
  hasValidNickname: boolean,
  isSubmitting: boolean,
  message: string,
  messageType: string,
  nickname: string,
  roomTypeOptions: RoomTypeOptions,
  roomTypeSelected: Array<string>,
  wasSubmitted: boolean,
  isDataPrivacyConfirmedChecked: boolean

}

const initialState = {
  diversitySelected: 'no',
  email: '',
  hasSelectedRoomType: false,
  hasValidEmail: false,
  hasValidNickname: false,
  isSubmitting: false,
  message: messages[messageTypes.EMPTY],
  messageType: messageTypes.EMPTY,
  nickname: '',
  roomTypeOptions: [],
  roomTypeSelected: [],
  wasSubmitted: false,
  isDataPrivacyConfirmedChecked: false
};

export class ApplicationFormContainer extends React.Component<Props, State> {
  state = initialState;

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.messageType) {
      return {
        ...prevState, ...nextProps,
        isSubmitting: false,
        wasSubmitted: true
      };
    }
    return {...prevState, ...nextProps};
  }

  componentDidMount() {
    this.props.fetchRoomTypes();
  }

  onNicknameChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const nickname = event.target.value;
    this.setState({
      ...this.state,
      nickname,
      hasValidNickname: isValidNickname(nickname)
    });
  };

  onEmailChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const email = event.target.value;
    this.setState({
      ...this.state,
      email,
      hasValidEmail: isValidEmail(email)
    });
  };

  onRoomTypeClick = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const index = this.state.roomTypeSelected.indexOf(event.target.value);
    const roomTypeSelected = index !== -1 ?
      [...this.state.roomTypeSelected.slice(0, index), ...this.state.roomTypeSelected.slice(index + 1)] :
      [...this.state.roomTypeSelected, event.target.value];
    this.setState({
      ...this.state,
      hasSelectedRoomType: roomTypeSelected.length > 0,
      roomTypeSelected
    });
  };

  onDiversityChange = (diversitySelected: string) => {
    this.setState({
      ...this.state,
      diversitySelected
    });
  };

  onSubmit = (event: SyntheticInputEvent<HTMLInputElement>) => {
    event.preventDefault();

    const {nickname, email, roomTypeSelected: roomTypes, diversitySelected: diversity} = this.state;

    this.setState({
      ...this.state,
      isSubmitting: true
    }, () => this.props.addApplication(nickname, email, roomTypes, diversity));
  };

  onDataPrivacyConfirmedChange = () => {
    this.setState({...this.state, isDataPrivacyConfirmedChecked: !this.state.isDataPrivacyConfirmedChecked});
  };

  render() {
    return this.state.wasSubmitted ? this.renderMessage() : this.renderForm();
  }

  renderForm = () => {
    const isEnabled = this.state.hasValidNickname &&
      this.state.hasValidEmail &&
      this.state.hasSelectedRoomType &&
      this.state.isDataPrivacyConfirmedChecked &&
      !this.state.isSubmitting;
    const values = {
      diversitySelected: this.state.diversitySelected,
      email: this.state.email,
      nickname: this.state.nickname,
      roomTypeSelected: this.state.roomTypeSelected
    };
    const handlers = {
      onEmailChange: this.onEmailChange,
      onNicknameChange: this.onNicknameChange,
      onRoomTypeClick: this.onRoomTypeClick,
      onDiversityChange: this.onDiversityChange,
      onSubmit: this.onSubmit
    };

    return (
      <ApplicationForm
        isDataPrivacyConfirmedChecked={this.state.isDataPrivacyConfirmedChecked}
        dataPrivacyConfirmedChange={this.onDataPrivacyConfirmedChange}
        handlers={handlers}
        hasValidEmail={this.state.hasValidEmail}
        hasValidNickname={this.state.hasValidNickname}
        isEnabled={isEnabled}
        roomTypeOptions={this.props.roomTypeOptions}
        values={values}
      />
    );
  };

  renderMessage = () => {
    return (
      <div
        id="application-message"
        className="alert alert-danger"
        role="alert">
        {this.state.message}
      </div>
    );
  };
}

const isValidNickname = (nickname: string) => nickname.trim().length > 0;
const isValidEmail = (email: string) => email.trim().length > 0 && isValidEmailFormat(email);

const mapStateToProps = state => {
  return {...state.application};
};

const mapDispatchToProps = {
  addApplication,
  fetchRoomTypes
};

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationFormContainer);