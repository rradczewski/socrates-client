//@flow

import applicationReducer from './applicationReducer';
import {addFailed, addSucceeded, roomTypesReceived} from '../../events/applicationEvents';
import {messages, messageTypes} from './applicationMessages';

const ROOM_TYPES = [
  {value: 'single', label: 'Single', prices: ['268€']},
  {value: 'junior', label: 'Junior (exclusively)', prices: ['234€']}
];

describe('ApplicationReducer', () => {
  describe('when roomTypes were received', () => {
    it('should set roomTypes in state', () => {
      const state = applicationReducer(undefined, roomTypesReceived(ROOM_TYPES));
      expect(state).toMatchObject({roomTypeOptions: ROOM_TYPES});
    });
  });

  describe('when application was added', () => {
    it('should set SUCCESS message in state', () => {
      const state = applicationReducer(undefined, addSucceeded());
      expect(state).toMatchObject(
        {message: messages.SUCCESS, messageType: messageTypes.SUCCESS});
    });
  });

  describe('when application failed', () => {
    it('should set FAIL message in state', () => {
      const state = applicationReducer(undefined, addFailed(messageTypes.FAIL));
      expect(state).toMatchObject(
        {message: messages.FAIL, messageType: messageTypes.FAIL});
    });
  });

  describe('when an application already existed', () => {
    it('should set ALREADY_APPLIED message in state', () => {
      const state = applicationReducer(undefined, addFailed(messageTypes.ALREADY_APPLIED));
      expect(state).toMatchObject(
        {message: messages.ALREADY_APPLIED, messageType: messageTypes.ALREADY_APPLIED});
    });
  });
});