// @flow

import React from 'react';
import ApplicationDescription from './description/ApplicationDescription';
import ApplicationFormContainer from './form/ApplicationFormContainer';

export default function Application() {
  return (
    <div id="application" className="container">
      <div className="row">
        <div className="col-md-12">
          <div className="page-header">
            <div className="btn-group pull-right hidden-print"/>
            <h1>Application</h1>
            <h2>SoCraTes, 23 - 26 August 2018</h2>
          </div>
        </div>
      </div>
      <ApplicationFormContainer />
      <ApplicationDescription />
    </div>
  );
}
