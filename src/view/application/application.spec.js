import React from 'react';
import {shallow} from 'enzyme';
import Application from './Application';

describe('(Component) Home', () => {
  const wrapper = shallow(<Application/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
