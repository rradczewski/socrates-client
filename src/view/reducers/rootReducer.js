// @flow

import authenticationReducer from './authenticationReducer';
import newsletterSignOutReducer from '../newsletter-signout/NewsletterSignOutReducer';
import newsletterSignUpReducer from '../home/newsletter/newsletterSignUpReducer';
import applicationReducer from '../application/form/applicationReducer';
import newsletterConfirmReducer from '../newsletter-confirm/NewsletterConfirmReducer';

type NewsletterState = {message: string};
const newsletterReducer = (state: NewsletterState, event: any) => {
  let nextState: $Shape<NewsletterState> = newsletterSignUpReducer(state, event);
  nextState = newsletterSignOutReducer(nextState, event);
  nextState = newsletterConfirmReducer(nextState, event);
  return nextState;
};

const reducerMap = {
  application: applicationReducer,
  authentication: authenticationReducer,
  newsletter: newsletterReducer
};

export default reducerMap;
