import reducer from './authenticationReducer';
import AuthenticationEvent from '../events/authenticationEvents';
import * as jwtStorage from '../services/jwtStorage';

describe('authentication reducer', () => {
  describe('with jwt token in local storage', () => {
    const getToken = jwtStorage.getToken;
    const TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9' +
      '.eyJlbWFpbCI6InRlc3RAdGVzdC5kZSIsIm5hbWUiOiJ0ZXN0IiwiaXNBZG1pbmlzdHJhdG9yIjp0cnVlfQ' +
      '.34sGwDqBKCYRI66Ewu_v_sTZJfN3_02Rjch3AD3kXBc';
    beforeEach(() => {
      jwtStorage.getToken = jest
        .fn()
        .mockImplementation(() => TOKEN);
    });
    afterEach(() => {
      jwtStorage.getToken = getToken;
    });

    it('token is used to initialize state', () => {
      const currentState = reducer(undefined, {});

      expect(currentState.token).toEqual(TOKEN);
      expect(currentState.isAdministrator).toBe(true);
      expect(currentState.email).toEqual('test@test.de');
    });
  });

  describe('on user login success', () => {
    let currentState;
    beforeEach(() => {
      const initialState = {
        token: '',
        isAdministrator: false,
        userName: '',
        hasFinished: false
      };
      currentState = reducer(initialState, {
        type: AuthenticationEvent.LOGIN_SUCCESS,
        token: 'theToken',
        data: {isAdministrator: false, name: 'UserName'}
      });
    });
    it('token is set', () => {
      expect(currentState.token).toEqual('theToken');
    });
    it('login has finished is true', () => {
      expect(currentState.hasFinished).toBe(true);
    });
    it('user is not an administrator', () => {
      expect(currentState.isAdministrator).toBe(false);
    });
    it('UserName is logged in', () => {
      expect(currentState.userName).toEqual('UserName');
    });
  });
  describe('on administrator login success', () => {
    let currentState;
    beforeEach(() => {
      const initialState = {
        token: '',
        userName: '',
        isAdministrator: false,
        hasFinished: false
      };
      currentState = reducer(initialState, {
        type: AuthenticationEvent.LOGIN_SUCCESS,
        token: 'theToken',
        data: {isAdministrator: true, name: 'UserName'}
      });
    });
    it('token is set', () => {
      expect(currentState.token).toEqual('theToken');
    });
    it('login has finished is true', () => {
      expect(currentState.hasFinished).toBe(true);
    });
    it('user is an administrator', () => {
      expect(currentState.isAdministrator).toBe(true);
    });
    it('UserName is logged in', () => {
      expect(currentState.userName).toEqual('UserName');
    });
  });
  describe('on login started', () => {
    let currentState;
    beforeEach(() => {
      const initialState = {
        token: '',
        isAdministrator: false,
        hasFinished: false
      };
      currentState = reducer(initialState, {type: AuthenticationEvent.LOGIN_STARTED});
    });
    it('token is empty', () => {
      expect(currentState.token).toEqual('');
    });
    it('login has finished is false', () => {
      expect(currentState.hasFinished).toBe(false);
    });
    it('user is not an administrator', () => {
      expect(currentState.isAdministrator).toBe(false);
    });
  });
  describe('on login error', () => {
    let currentState;
    beforeEach(() => {
      const initialState = {
        token: '',
        isAdministrator: false,
        hasFinished: false
      };
      currentState = reducer(initialState, {type: AuthenticationEvent.LOGIN_ERROR});
    });
    it('token is empty', () => {
      expect(currentState.token).toEqual('');
    });
    it('login has finished is true', () => {
      expect(currentState.hasFinished).toBe(true);
    });
    it('user is not an administrator', () => {
      expect(currentState.isAdministrator).toBe(false);
    });
  });

  describe('on logout success', () => {
    let currentState;
    beforeEach(() => {
      const initialState = {
        token: '',
        isAdministrator: false,
        hasFinished: false
      };
      currentState = reducer(initialState, {type: AuthenticationEvent.LOGIN_STARTED});
    });
    it('token is empty', () => {
      expect(currentState.token).toEqual('');
    });
    it('login has finished is false', () => {
      expect(currentState.hasFinished).toBe(false);
    });
    it('user is not an administrator', () => {
      expect(currentState.isAdministrator).toBe(false);
    });
  });
});
