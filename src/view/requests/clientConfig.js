import * as axios from 'axios/index';

export const setAuthorizationHeader = token =>
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

export const removeAuthorizationHeader = () =>
  delete axios.defaults.headers.common['Authorization'];
