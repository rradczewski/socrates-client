// @flow

import './faq.css';
import React from 'react';
import FAQItem from './FAQItem';
import faq from './faqItems';
import Sponsoring from '../common/sponsoring/Sponsoring';

export default function FAQ() {
  return (
    <div id="faq" className="container">
      <div className="row">
        <div className="col-md-12">
          <div className="page-header"><h1>FAQ</h1></div>
        </div>
      </div>
      <div className="row">
        <div id="faq-accordion" className="col-md-8">
          <div className="row">
            <div className="col-md-12">
              <div className="page-header"><h3>FAQ</h3></div>
            </div>
          </div>
          {faq.map((item, index) =>
            <FAQItem parentId="faq-accordion" title={item.title} content={item.content} index={index}
              key={`faq-item-${index}`}/>
          )}
        </div>
        <div className="logocolumn col-sm-4 col-md-3 col-lg-4 col-md-offset-1 col-lg-offset-0">
          <div className="sidebar">
            <Sponsoring/>
          </div>
        </div>
      </div>
    </div>
  );
}

