import {Link} from 'react-router-dom';
import React from 'react';

const faqItems = [
  {
    title: 'I have kids, and maybe a partner. Can they come?',
    content:
      <p>Great question, thank you for that! We would love to provide help on that and make arrangements, however we
        need to have an idea on how many people/kids we would need to accommodate. Please give us a shout (e.g.
        email/slack), and you should find an option in the form after you won a seat in the lottery. </p>
  },
  {
    title: 'How can I account the expenses when I\'m self-employed?',
    content: <p>The hotel will issue an invoice for the full costs at the check-out.</p>
  },
  {
    title: 'When\'s the best time to arrive on Thursday?',
    content:
      <div>
        <p>
          Conference starts at ~5pm. In order to enjoy a relaxed start into the conference, it&#39;s best to arrive
          early enough to check-in, leave your luggage in your room, get a cold drink and start having some interesting
          conversations all before 5pm. </p>
        <p>
          However, be aware that the evening&#39;s buffet will be served from 6pm to 7.30pm and that the official
          World Cafe will kick off the conference at 7.30pm. </p>
      </div>
  },
  {
    title: 'What time does the conference end on Saturday?',
    content:
      <p>
        The closing of Saturday&#39;s sessions will start at 4.30pm, and will be followed by a joint retrospective,
        which usually lasts ~1h. Without giving any guarantees, it&#39;s likely that the conference will be over at
        around 5-5.30pm. </p>
  },
  {
    title: 'What time do the workshops end on Sunday?',
    content:
      <p>On Sunday, all workshops must end by 6pm. However, each workshop has the freedom to determine when it ends as
        long as the 6pm deadline is respected.</p>
  },
  {
    title: 'Is SoCraTes the best conference ever?',
    content: <p>Funny you should ask. Yes, certainly, because you make it the best it can be for yourself.</p>
  },
  {
    title: 'How do I get in touch with the organizers?',
    content:
      <div>
        <p>
          You can ask your questions by sending an e-mail to <a
            href="mailto://info@socrates-conference.de">info@socrates-conference.de</a>. </p>
        <p>
          Another option is to use our slack channel: <a
            href="https://socrates-conference.slack.com">https://socrates-conference.slack.com</a>
        </p>
        <p>
          If you didn&#39;t get invited yet, just email us: <a
            href="mailto:info@socrates-conference.de">info@socrates-conference.de</a>
        </p>
      </div>
  },
  {
    title: 'Where can I talk to other participants?',
    content:
      <div>
        <p>
          Please use our slack channel: <a
            href="https://socrates-conference.slack.com">https://socrates-conference.slack.com</a>
        </p>
        <p>
          There you can talk to other participants about all sorts of topics. For example, to find a roommate or share a
          ride by train, car or taxi on your way to SoCraTes or back home.</p>
        <p>
          If you didn&#39;t get invited yet, just email us: <a
            href="mailto:info@socrates-conference.de">info@socrates-conference.de</a>
        </p>
      </div>
  },
  {
    title: 'How will I get to SoCraTes 2018?',
    content:
      <div>
        <p>
          SoCraTes 2018 again takes place at
          &nbsp;<a href="http://www.hotel-park-soltau.de/" target="_blank" rel="noopener noreferrer">
            Hotel Park Soltau (website only in german)
          </a>.
        </p>
        <p>
          <strong>Need some help finding the way </strong>? Check out the <Link to="/location">location page </Link> for
          the details about the conference&#39;s venue. </p>
      </div>
  }
];

export default faqItems;
