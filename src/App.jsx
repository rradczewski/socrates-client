// @flow

import * as React from 'react';
import * as PropTypes from 'prop-types';
import './App.css';
import {connect, Provider} from 'react-redux';
import {Redirect, Route, Router, Switch} from 'react-router-dom';
import {Home} from './view/home/Home';
import Format from './view/format/Format';
import Values from './view/values/Values';
import Location from './view/location/Location';
import Accessibility from './view/accessibility/Accessibility';
import Imprint from './view/imprint/Imprint';
import Sponsoring from './view/sponsoring/Sponsoring';
import ScrollToTop from './view/common/ScrollToTop';
import History from './view/history/History';
import FAQ from './view/faq/FAQ';
import Application from './view/application/Application';
import Header from './view/common/Header';
import Navigation from './view/common/navigation/Navigation';
import Footer from './view/common/Footer';
import NewsletterSignOutContainer from './view/newsletter-signout/NewsletterSignOutContainer';
import WebsiteMonitoring from './WebsiteMonitoring';
import FourOFour from './view/common/FourOFour';
import NewsletterConfirmContainer from './view/newsletter-confirm/NewsletterConfirmContainer';
import PrivacyPolicy from './view/privacyPolicy/PrivacyPolicy';

export type WithLayoutProps = {
  children: React.Node,
}

function WithLayout(props: WithLayoutProps) {
  return (
    <div>
      <Navigation/>
      {props.children}
      <Footer/>
    </div>
  );
}

WithLayout.propTypes = {
  children: PropTypes.node.isRequired
};

export type AppProps = {
  store: any,
  history: any,
}

export class App extends React.Component<AppProps> {
  static propTypes = {
    history: PropTypes.any,
    store: PropTypes.any
  };

  static defaultProps = {};

  render() {
    return (
      <Provider store={this.props.store}>
        <Router history={this.props.history}>
          <WebsiteMonitoring>
            <ScrollToTop>
              <Switch>
                <Redirect exact from="/" to="/home"/>
                <Route exact path="/home">
                  <WithLayout>
                    <Header/>
                    <Home/>
                  </WithLayout>
                </Route>
                <Route exact path="/format">
                  <WithLayout>
                    <Format/>
                  </WithLayout>
                </Route>
                <Route exact path="/location">
                  <WithLayout>
                    <Location/>
                  </WithLayout>
                </Route>
                <Route exact path="/values">
                  <WithLayout>
                    <Values/>
                  </WithLayout>
                </Route>
                <Route exact path="/accessibility">
                  <WithLayout>
                    <Accessibility/>
                  </WithLayout>
                </Route>
                <Route exact path="/imprint">
                  <WithLayout>
                    <Imprint/>
                  </WithLayout>
                </Route>
                <Route exact path="/privacy-policy">
                  <WithLayout>
                    <PrivacyPolicy/>
                  </WithLayout>
                </Route>
                <Route exact path="/sponsoring">
                  <WithLayout>
                    <Sponsoring/>
                  </WithLayout>
                </Route>
                <Route exact path="/application">
                  <WithLayout>
                    <Application/>
                  </WithLayout>
                </Route>
                <Route exact path="/history">
                  <WithLayout>
                    <History/>
                  </WithLayout>
                </Route>
                <Route exact path="/faq">
                  <WithLayout>
                    <FAQ/>
                  </WithLayout>
                </Route>
                <Route exact path="/newsletter-sign-out/:data?">
                  <WithLayout><NewsletterSignOutContainer/></WithLayout>
                </Route>
                <Route exact path="/newsletter-confirm/:data?">
                  <WithLayout><NewsletterConfirmContainer/></WithLayout>
                </Route>
                <Route>
                  <WithLayout>
                    <FourOFour/>
                  </WithLayout>
                </Route>
              </Switch>
            </ScrollToTop>
          </WebsiteMonitoring>
        </Router>
      </Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {...state};
};

export default connect(mapStateToProps)(App);
