// @flow

import React, {Component} from 'react';

let ineum;

export default class WebsiteMonitoring extends Component<{children: any}> {

  componentWillMount() {
    ineum = global.ineum;
    ineum('page', '/' + window.location.pathname.split('/')[1]);
    ineum('startSpaPageTransition');
  }

  componentDidMount() {
    ineum('meta', 'content', '/' + window.location.pathname.split('/')[1]);
    ineum('endSpaPageTransition');
  }

  componentWillUpdate() {
    ineum('page', '/' + window.location.pathname.split('/')[1]);
    ineum('startSpaPageTransition');
  }

  componentDidUpdate() {
    ineum('meta', 'content', '/' + window.location.pathname.split('/')[1]);
    ineum('endSpaPageTransition', {
      status: 'completed',
      url: window.location.href
    });
  }

  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}