# Contributing

First of all, thank you for taking the time to contribute to the web application and SoCraTes conference.

What follows is a set of guidelines for contributing. Those are not rules so use your best judgement.

Also, feel free to propose changes to this document too.

## Application

The application is split into two repositories
- [Backend](https://gitlab.com/socrates-conference/socrates-server), built using Node.js and Flow
- [Frontend](https://gitlab.com/socrates-conference/socrates-client), built using React / Redux and Flow

CI automatically deploys

| Branch  | Environment | Address                                                                           |
|---------|-------------|-----------------------------------------------------------------------------------|
| develop | staging     | [click open](https://gitlab.com/socrates-conference/socrates-client/environments) |
| master  | production  | https://socrates-conference.de                                                    |

Staging database:
- address: 35.158.56.167:3333
- database: socrates_db
- user: socrates
- password: eL,jW+6le7Q0mvR,

You can access it from a terminal with
```
mysql --host=35.158.56.167 --port=3333 --user=socrates --password=eL,jW+6le7Q0mvR, socrates_db
```

## Design

During SoCraTes 2017 a group of brave people did an Event Storming for the app. Review it before implementing a new feature:
- [Big Picture](./event_storming/big_picture.pdf)
- [Aggregates](./event_storming/aggregates.pdf)

## Requirements

- NodeJS v8.x (If you're on Linux/Mac, We strongly recommend using [NVM](https://github.com/creationix/nvm) to manage your node installations. There also is an [NVM for Windows](https://github.com/coreybutler/nvm-windows), but that's a different project - no guarantees. )
- [Yarn](https://yarnpkg.com/) (`npm install -g yarn`)
- `npm install -g flow-typed && flow-typed install jest@21.x.x` (should match jest version used in the project)

## Development

- `yarn` to install the app dependencies
- `yarn start` to run the application
- `yarn test:unit` or `yarn test:unit -t NAME` to run unit tests
- `yarn test:cucumber` to run Cucumber tests
- `yarn precommit` to run all tests and checks (check `package.json` to see how to run a subset of them)

## How to contribute

- Report bugs: open an issue with a clear description of the problem and
  how to reproduce (sometimes a [gif](https://giphy.com/) is worth a thousand words)
- Participate to discussions in #neogora on [Slack](http://socrates-conference.slack.com/)
- Participate to discussions in the issues
- Review non wip Merge Requests
- Create a Merge Request

## Merge Request process

### As an author

- Join the [Gitlab group](https://gitlab.com/socrates-conference/), an admin will accept the request asap; this is needed because the CI runners are restricted to the project
- Branch out of `develop`
- Create a WIP Merge Request (add "WIP:" at the beginning of the title) with `develop` as target branch; we always merge first to `develop` to be able to test in the staging environment
- Explain as clearly as possible what the Merge Request does (consider adding screenshots or gifs)
- Write tests and code to implement the feature; try to keep the Merge Request as small as possible (~200 LOCs would be great): the smaller the Merge Request the easier to write for the author and review for the reviewer. If needed, split the feature into multiple Merge Requests.
- When ready and `yarn precommit` is happy, remove the WIP status and ask somebody to review and merge

### As a reviewer

- Comment as clearly as possible (consider adding snippets of code)
- Check `yarn precommit` before merging. This is because in feature branches we run a subset of `yarn precommit` to save CI runner time. At the same time, on `develop` and `master` we run `yarn precommit` entirely
- After merging a Merge Request, close all related issues
- Check that the build is green on CI

## Issues

We keep all issues in the [client repository](https://gitlab.com/socrates-conference/socrates-client/issues). Please use the right tag to specify if the issue refers to the ~Server, ~Client or both repositories. Also, whenever working on an issue, assign yourself and tag it as ~Doing

~"Good first issue" are a perfect starting point for new contributors.

