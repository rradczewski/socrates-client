# language:en
Feature: Application
  As an Applicant,
  I want to sign up for the SoCraTes lottery,
  So that I have a chance to get a ticket.

  Background:
    Given the application is running

  @apiIsUp
  Scenario Outline: Application succeeds

    Given the landing-page is open
    And the button ".btn-application" was clicked
    When I enter "<nickname>" and "<email>"
    And I check "single"
    And I check "junior"
    And I select "yes"
    And select the checkbox "#application-dataPrivacy"
    And I send my application
    Then a message "<message>" is shown

    Examples:
      | nickname | email | message |
      | Jan3 D0e | jane@doe.com | Thank you for caring about Software Craft. You are now taking part in the lottery for SoCraTes. |


  @apiIsDown
  Scenario Outline: Application fails

    Given the landing-page is open
    And the button ".btn-application" was clicked
    When I enter "<nickname>" and "<email>"
    And I check "single"
    And I check "junior"
    And I select "yes"
    And select the checkbox "#application-dataPrivacy"
    And I send my application
    Then a message "<message>" is shown

    Examples:
      | nickname | email | message |
      | Jane Doe | jane@doe.com | Unfortunately the api is down, please try again later. |
