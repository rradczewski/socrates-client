/* eslint-disable babel/no-invalid-this */
import {defineSupportCode} from 'cucumber';
import {push} from 'react-router-redux';
import {expectContentToContain} from '../support/expectations';
import expect from 'expect';

// noinspection JSUnusedGlobalSymbols
function setInputValue(wrapper, selector, value) {
  const nameNode = wrapper.find(selector);
  nameNode.instance().value = value;
  nameNode.simulate('change', {target: {value: value}});
}

defineSupportCode(function ({Given, When, Then}) {
  Given('the {string} page is open', function (title) {
    this.sagaTester.dispatch(push(`/${title}`));
  });

  When('I open the {string} page', function (title) {
    this.sagaTester.dispatch(push(`/${title}`));
  });

  When('enter {string} and {string} in the newsletter registration form', function (name, email) {
    setInputValue(this.wrapper, '#newsletter-name', name);
    setInputValue(this.wrapper, '#newsletter-email', email);
    this.wrapper.update();
  });

  When('newsletter registration service unavailable', function () {
    this.promise = Promise.resolve(false);
    this.addInterestedPersonStub.returns(this.promise);
  });

  When('newsletter registration service available', function () {
    this.promise = Promise.resolve(true);
    this.addInterestedPersonStub.returns(this.promise);
  });

  When('click the newsletter registration button', function () {
    const nameNode = this.wrapper.find('#newsletter-form-button');
    nameNode.simulate('click');
  });

  Then('it shows the {string} {string}', function (selector, content) {
    expectContentToContain(selector, content);
  });

  Then('the message {string} is shown', function (message) {
    return this.promise.then(() => {
      this.wrapper.update();
    }).then(() => {
      const messageNode = this.wrapper.find('#newsletter-message');
      expect(messageNode.length).toBe(1);
      expect(messageNode.instance().innerHTML).toEqual(message);
    });
  });
});
