/* eslint-disable babel/no-invalid-this */
import {defineSupportCode} from 'cucumber';
import {push} from 'react-router-redux';
import ReactTestUtils from 'react-dom/test-utils';
import {expectContentToContain} from '../support/expectations';
import {select} from '../support/cssSelectors';
import {stub} from 'sinon';
import api from '../../src/view/requests/api';

defineSupportCode(function ({Before, After, Given, When, Then}) {
  Before({tags: '@apiIsUp'}, function() {
    this.addApplication = stub(api, 'addApplication').returns(Promise.resolve());
  });

  Before({tags: '@apiIsDown'}, function() {
    this.addApplication = stub(api, 'addApplication').rejects();
  });

  Before({tags: '@apiIsUp or @apiIsDown'}, function() {
    const ROOM_TYPES = [
      {value: 'single', label:'Single', prices: ['268€']},
      {value: 'junior', label:'Junior (exclusively)', prices: ['234€']}
    ];

    const getRoomTypeOptions = Promise.resolve(ROOM_TYPES);
    this.getRoomTypeOptions = stub(api, 'getRoomTypeOptions').returns(getRoomTypeOptions);
  });

  After({tags: '@apiIsUp or @apiIsDown'}, function() {
    this.getRoomTypeOptions.restore();
    this.addApplication.restore();
  });

  Given('the landing-page is open', function () {
    this.sagaTester.dispatch(push('/'));
  });

  Given('the button {string} was clicked', function (klass) {
    if (klass === '.btn-application') {
      this.sagaTester.dispatch(push('/application')); // TODO: click button instead of changing route
    }
  });

  Given('I click the button {string}', function (klass) {
    click(klass);
    return Promise.resolve();
  });

  When('I enter {string} and {string}', function (nickname, email) {
    setInputValue('#nickname', nickname);
    setInputValue('#email', email);
  });

  When('I check {string}', function (value) {
    return Promise.resolve().then(() => {
      click(`input[type=checkbox][value=${value}]`);
    });
  });

  When('I select {string}', function (value) {
    return Promise.resolve().then(() => {
      click(`input[type=radio][value=${value}]`);
    });
  });

  When('I send my application', function () {
    click('#submit-application');
    return Promise.resolve();
  });

  Then('a message {string} is shown', function (message) {
    return Promise.resolve().then(() => {
      expectContentToContain('#application-message', message);
    });
  });
});

function click(selector) {
  select(selector).click();
}

function setInputValue(selector, value) {
  const input = select(selector);
  input.value = value;
  ReactTestUtils.Simulate.change(input, {target: {value}});
  ReactTestUtils.Simulate.blur(input);
}
