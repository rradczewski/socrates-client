// @flow
import expect from 'expect';
import {select, selectAll} from './cssSelectors';
import {nodeListToArray} from './conversions';

// noinspection JSUnusedGlobalSymbols
export function expectContentToEqual(selector: string, content: string): void {
  const container = select(selector);
  expect(container).not.toBeNull();
  // $FlowFixMe because container is now never null
  expect(container.innerHTML).toEqual(content);
}

export function expectContentToContain(selector: string, content: string): void {
  const containers: ?NodeList<HTMLElement> = selectAll(selector);
  expect(containers).toBeDefined();

  const containerArray = nodeListToArray(containers);
  expect(containerArray.filter(container => container.innerHTML === content)).toHaveLength(1);
}

// noinspection JSUnusedGlobalSymbols
export function expectValueToEqual(selector: string, value: string): void {
  const element = select(selector);
  expect(element).not.toBeNull();
  // $FlowFixMe because element is now never null
  expect(element.value).toEqual(value);
}

// noinspection JSUnusedGlobalSymbols
export function expectElementIsMissing(selector: string): void {
  expect(select(selector)).toBeNull();
}

// noinspection JSUnusedGlobalSymbols
export function expectElementExists(selector: string): void {
  expect(select(selector)).not.toBeNull();
}
