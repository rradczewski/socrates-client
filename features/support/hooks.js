/* eslint-disable babel/no-invalid-this */
import {defineSupportCode} from 'cucumber';
import setUpJsDom from '../../src/test/setupJsDom';
import createTestLogger from '../../src/test/logger';
import rootSaga from '../../src/view/sagas/rootSaga';
import {combineReducers} from 'redux';
import {routerMiddleware} from 'react-router-redux';
import SagaTester from 'redux-saga-tester';
import {stub} from 'sinon';
import api from '../../src/view/requests/api';

defineSupportCode(function ({Before, After}) {
  Before(function () {
    this.sagaTester = new SagaTester({
      reducers: combineReducers(this.allReducers),
      middlewares: [routerMiddleware(this.history), createTestLogger()]
    });
    this.sagaTester.start(rootSaga);
    this.root = setUpJsDom();
    this.addInterestedPersonStub = stub(api, 'addInterestedPerson');
  });

  After(function () {
    if (this.root) {
      this.root = undefined;
    }
    api.addInterestedPerson.restore();
  });
});
