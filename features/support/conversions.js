// @flow

export const nodeListToArray = (list: ?NodeList<HTMLElement>): HTMLElement[] => {
  const arr = [];
  if (list) {
    for (const node of list.values()) {
      arr.push(node);
    }
  }
  return arr;
};